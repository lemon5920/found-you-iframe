function initializeIframe (iframe) {
  const intersectionObserver = new IntersectionObserver(function foundIframe(entries) {
    // If intersectionRatio is 0, the target is out of view
    // and we do not need to do anything.
    if (entries[0].intersectionRatio <= 0) return;

    // Show alert for one time.
    intersectionObserver.disconnect();
    alert('Hey, You found me!');
  })
  // start observing
  intersectionObserver.unobserve(iframe.frameElement);
  intersectionObserver.observe(iframe.frameElement);
}

initializeIframe(window);
